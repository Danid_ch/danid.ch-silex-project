<?php

namespace Danid3\Controller;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Danid3\Domain\Contact;
use Danid3\Form\Type\ContactType;

class HomeController {

	public function indexAction(Application $app) {
		$projects = $app['dao.project']->findAllPublished();
		$projTechs = $app['dao.projecttechnology']->findAll();
		$projServs = $app['dao.projectservice']->findAll();
		$images = $app['dao.image']->findAll();
		return $app['twig']->render('index.html.twig', array(
			'projects' => $projects,
			'projtechs' => $projTechs,
			'projservs' => $projServs,
			'images' => $images
		));
	}

	public function portfolioAction(Application $app) {
		return $app->redirect('/');
	}

	public function meAction(Application $app) {
		return $app['twig']->render('me.html.twig');
	}

	public function contactAction(Request $request, Application $app) {
		$contact = new Contact();
		$contactForm = $app['form.factory']->create(new ContactType(), $contact);
		$contactForm->handleRequest($request);
		if($contactForm->isSubmitted() && $contactForm->isValid()){
			// add code for mail sending here
			$message = \Swift_Message::newInstance()
				->setSubject($contact->getSubject())
				->setFrom(array($contact->getEmail()))
				->setTo(array('contact-me@danid.ch'))
				->setBody($app['twig']->render('email.txt.twig', array('contact' => $contact)));
			$sent = $app['mailer']->send($message);
			if ($sent) {
				$app['session']->getFlashBag()->add('success', 'Votre message a bien été envoyé!');
			} else {
				$app['session']->getFlashBag()->add('error', 'Votre e-mail n\'a pas été envoyé, veuillez réessayer plus tard SVP.');
			}
		}
		return $app['twig']->render('contact.html.twig', array('contactForm' => $contactForm->createView()));
	}
}
