<?php

namespace Danid3\Controller;

use Silex\Application;
use Danid3\Domain\ProjectService;

class ProjectServiceController {

	public function addAction(Application $app, $proj_id, $serv_id) {
		$project = $app['dao.project']->findById($proj_id);
		$service = $app['dao.service']->findById($serv_id);

		$projServ = new ProjectService();
		$projServ->setProject($project);
		$projServ->setService($service);

		$app['dao.projectservice']->save($projServ);
		$app['session']->getFlashBag()->add('success', 'Lien projet-service créé.');
		return $app->redirect('/admin/project/adddetails/'.$proj_id);
	}

	public function deleteAction(Application $app, $projserv_id) {
		$projserv = $app['dao.projectservice']->findById($projserv_id);
		$app['dao.projectservice']->delete($projserv_id);
		$app['session']->getFlashBag()->add('success', 'Lien projet-service supprimé.');

		return $app->redirect('/admin/project/adddetails/'.$projserv->getProject()->getId());
	}
}
