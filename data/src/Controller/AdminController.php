<?php

namespace Danid3\Controller;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

class AdminController {

	public function indexAction(Application $app) {
		$projects = $app['dao.project']->findAll();
		$users = $app['dao.user']->findAll();
		$technologies = $app['dao.technology']->findAll();
		$services = $app['dao.service']->findAll();
		$images = $app['dao.image']->findAll();

		return $app['twig']->render('admin.html.twig', array(
			'users' => $users,
			'projects' => $projects,
			'technologies' => $technologies,
			'services' => $services,
			'images' => $images
		));
	}

	public function loginAction(Request $request, Application $app) {
		return $app['twig']->render('login.html.twig', array(
            'error'         => $app['security.last_error']($request),
            'last_username' => $app['session']->get('_security.last_username'),
            ));
	}
}
