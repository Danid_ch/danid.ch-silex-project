<?php

namespace Danid3\Controller;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Danid3\Domain\Project;
use Danid3\Form\Type\ProjectType;

class ProjectController {

	public function showAction($slug, Application $app) {
		$project = $app['dao.project']->findBySlug($slug);
		$images = $app['dao.image']->findAllByProject($project->getId());
		$projTechs = $app['dao.projecttechnology']->findAllByProject($project->getId());
		$projServs = $app['dao.projectservice']->findAllByProject($project->getId());
		return $app['twig']->render('project.html.twig', array(
			'project' => $project,
			'images' => $images,
			'projtechs' => $projTechs,
			'projservs' => $projServs
		));
	}

    public function addAction(Request $request, Application $app) {
    	$project = new Project();
		$projectForm = $app['form.factory']->create(new ProjectType(), $project);
		$projectForm->handleRequest($request);
		if ($projectForm->isSubmitted() && $projectForm->isValid()) {
			$project->setSlug($app['slugify']->slugify($project->getName()));
			$app['dao.project']->save($project);
			$app['session']->getFlashBag()->add('success', 'Le project à bien été enregistré.');
		}
		return $app['twig']->render('project_form.html.twig', array(
			'title' => 'Nouveau projet',
			'projectForm' => $projectForm->createView()));
	}

	public function addDetailsAction($id, Application $app) {
		$project = $app['dao.project']->findById($id);
		$nonRelatedTechs = $app['dao.technology']->findAllNotInProject($id);
		$nonRelatedServs = $app['dao.service']->findAllNotInProject($id);
		$projTechs = $app['dao.projecttechnology']->findAllByProject($id);
		$projServs = $app['dao.projectservice']->findAllByProject($id);
		$images = $app['dao.image']->findAllByProject($id);

		return $app['twig']->render('project_details.html.twig', array(
			'project' => $project,
			'nonRelatedTechs' => $nonRelatedTechs,
			'nonRelatedServs' => $nonRelatedServs,
			'projservs' => $projServs,
			'projtechs' => $projTechs,
			'images' => $images
		));
	}

	public function editAction($id, Request $request, Application $app) {
		$project = $app['dao.project']->findById($id);
		$projectForm = $app['form.factory']->create(new ProjectType(), $project);
		$projectForm->handleRequest($request);
		if ($projectForm->isSubmitted() && $projectForm->isValid()) {
			$project->setSlug($app['slugify']->slugify($project->getName()));
			$app['dao.project']->save($project);
			$app['session']->getFlashBag()->add('success', 'Le projet à bien été modifié.');
		}
		return $app['twig']->render('project_form.html.twig', array(
			'title' => 'Modifier le projet '.$project->getName(),
			'projectForm' => $projectForm->createView(),
			'project' => $project
		));
	}

	public function deleteAction($id, Application $app) {
		$project = $app['dao.project']->findById($id);
		$app['dao.projecttechnology']->deleteByProjectId($id);
		$app['dao.projectservice']->deleteByProjectId($id);

		$dir = __DIR__.'/../../web/img/'.$project->getSlug();
		if(is_dir($dir)){
			$it = new RecursiveDirectoryIterator($dir, RecursiveDirectoryIterator::SKIP_DOTS);
			$files = new RecursiveIteratorIterator($it,
			             RecursiveIteratorIterator::CHILD_FIRST);
			foreach($files as $file) {
			    if ($file->isDir()){
			        rmdir($file->getRealPath());
			    } else {
			        unlink($file->getRealPath());
			    }
			}
			rmdir($dir);
		}

		$app['dao.image']->deleteByProjectId($id);
		$app['dao.project']->delete($id);
		$app['session']->getFlashBag()->add('success', 'Le projet à bien été supprimé.');

		return $app->redirect('/admin');
	}

	public function publishAction($id, Application $app) {
		$project = $app['dao.project']->findById($id);
		$project->setIsPublished(true);
		$app['dao.project']->save($project);

		return $app->redirect('/admin');
	}

	public function unpublishAction($id, Application $app) {
		$project = $app['dao.project']->findById($id);
		$project->setIsPublished(false);
		$app['dao.project']->save($project);

		return $app->redirect('/admin');
	}
}
