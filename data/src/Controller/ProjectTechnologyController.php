<?php

namespace Danid3\Controller;

use Silex\Application;
use Danid3\Domain\ProjectTechnology;

class ProjectTechnologyController {

	public function addAction(Application $app, $id_proj, $id_tech) {
		$project = $app['dao.project']->findById($id_proj);
		$technology = $app['dao.technology']->findById($id_tech);
		
		$projTech = new ProjectTechnology();
		$projTech->setProject($project);
		$projTech->setTechnology($technology);

		$app['dao.projecttechnology']->save($projTech);
		$app['session']->getFlashBag()->add('success', 'Lien projet-technologie créé.');
		return $app->redirect('/admin/project/adddetails/'.$id_proj);
	}

	public function deleteAction(Application $app, $projtech_id) {
		$projtech = $app['dao.projecttechnology']->findById($projtech_id);
		$app['dao.projecttechnology']->delete($projtech_id);
		$app['session']->getFlashBag()->add('success', 'Lien projet-technologie supprimé.');

		return $app->redirect('/admin/project/adddetails/'.$projtech->getProject()->getId());
	}
}
