<?php

namespace Danid3\Controller;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Danid3\Domain\Service;
use Danid3\Form\Type\ServiceType;

class ServiceController {

	public function addAction(Request $request, Application $app) {
		$service = new Service();
		$serviceForm = $app['form.factory']->create(new ServiceType(), $service);
		$serviceForm->handleRequest($request);

		if($serviceForm->isSubmitted() && $serviceForm->isValid()){
			$app['dao.service']->save($service);
			$app['session']->getFlashBag()->add('success', 'Le service à bien été créé.');
		}
		return $app['twig']->render('tech_service_form.html.twig', array(
			'title' => 'Nouveau service',
			'techServiceForm' => $serviceForm->createView()
		));
	}

	public function editAction(Request $request, Application $app, $id) {
		$service = $app['dao.service']->findById($id);
		$serviceForm = $app['form.factory']->create(new ServiceType(), $service);
		$serviceForm->handleRequest($request);

		if($serviceForm->isSubmitted() && $serviceForm->isValid()){
			$app['dao.service']->save($service);
			$app['session']->getFlashBag()->add('success', 'Le service à bien été modifié.');
		}
		return $app['twig']->render('tech_service_form.html.twig', array(
			'title' => 'Editer service',
			'techServiceForm' => $serviceForm->createView()
		));
	}

	public function deleteAction(Application $app, $serv_id) {
		$app['dao.projectservice']->deleteByServiceId($serv_id);
		$app['dao.service']->delete($serv_id);
		$app['session']->getFlashBag()->add('success', 'Le service à bien été supprimé.');

		return $app->redirect('/admin');
	}
}
