<?php

namespace Danid3\Controller;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Danid3\Domain\Image;
use Danid3\Form\Type\ImageType;

class ImageController {

	public function addAction(Request $request, Application $app, $proj_id) {
		$image = new Image();
		$project = $app['dao.project']->findById($proj_id);
		$image->setProject($project);
		$imageForm = $app['form.factory']->create(new ImageType(), $image);
		$imageForm
			->add('filename', 'file', array('data_class' => null));

		$imageForm->handleRequest($request);
		if ($imageForm->isSubmitted() && $imageForm->isValid()) {
			$filename = $imageForm['filename']->getData()->getClientOriginalName();
			$filenameExtension = $imageForm['filename']->getData()->guessExtension();
			$slugifiedFilename = $app['slugify']->slugify($filename);
			$image->setFilename($slugifiedFilename.'.'.$filenameExtension);

			$projectFolder = __DIR__.'/../../../web/img/'.$image->getProject()->getSlug();
			if(!file_exists($projectFolder.'/')){
				mkdir($projectFolder, 0775);
				mkdir($projectFolder.'/fullsize/', 0775);
				mkdir($projectFolder.'/reducedsize/', 0775);
			}

			$imageForm['filename']->getData()->move($projectFolder.'/fullsize/', $image->getFilename());


			// image reduction sequence
			$sourceImagePath = $projectFolder.'/fullsize/'.$image->getFilename();
			$resizedImagePath = $projectFolder.'/reducedsize/'.$image->getFilename();
			$maxWidth = 730;
			$maxHeight = 400;
			$imageSize = getimagesize($sourceImagePath);
			if ($imageSize[0] > $maxWidth || $imageSize[1] > $maxHeight) {
				// find smallest ratio to set best size for picture
				$smallestRatio = min($maxWidth/$imageSize[0], $maxHeight/$imageSize[1]);
				$newWidth = ceil($imageSize[0]*$smallestRatio);
				$newHeigth = ceil($imageSize[1]*$smallestRatio);

				$resizedImage = imagecreatetruecolor($newWidth, $newHeigth);
				$sourceImage = imagecreatefromstring(file_get_contents($sourceImagePath));

				imagecopyresampled($resizedImage, $sourceImage, 0, 0, 0, 0, $newWidth, $newHeigth, $imageSize[0], $imageSize[1]);

				switch ($imageSize['mime']) {
					case 'image/jpeg':
						imagejpeg($resizedImage, $resizedImagePath, 100);
						break;

					case 'image/png':
						imagepng($resizedImage, $resizedImagePath, 0);
						break;

					case 'image/gif':
						imagepng($resizedImage, $resizedImagePath);
						break;
					
					default:
						throw new \Exception("No matching mime type ".$imageSize['mime']);
						break;
				}

				imagedestroy($resizedImage);
				imagedestroy($sourceImage);
			}

			$app['dao.image']->save($image);
			$app['session']->getFlashBag()->add('success', 'L\'image à bien été ajoutée.');
		}

		return $app['twig']->render('image_form.html.twig', array(
			'title' => 'Ajouter une image pour le projet: ',
			'imageForm' => $imageForm->createView(),
			'project' => $project
		));
	}

	public function editAction(Request $request, Application $app, $id) {
		$image = $app['dao.image']->findById($id);
		$imageForm = $app['form.factory']->create(new ImageType(), $image);
		$project = $image->getProject();

		$imageForm->handleRequest($request);
		if ($imageForm->isSubmitted() && $imageForm->isValid()) {
			$app['dao.image']->save($image);
			$app['session']->getFlashBag()->add('success', 'L\'image à bien été modifiée.');
		}

		return $app['twig']->render('image_edit_form.html.twig', array(
			'title' => 'Modifier l\'image pour le projet: ',
			'imageForm' => $imageForm->createView(),
			'project' => $project
		));
	}

	public function deleteAction(Application $app, $id) {
		$image = $app['dao.image']->findById($id);

		$project_id = $image->getProject()->getId();
		
		$projectFolder = __DIR__.'/../../../web/img/'.$image->getProject()->getSlug();
		// Delete image reduction and original file
		$sourceImagePath = $projectFolder.'/fullsize/'.$image->getFilename();
		$resizedImagePath = $projectFolder.'/reducedsize/'.$image->getFilename();
		unlink($sourceImagePath);
		unlink($resizedImagePath);

		$app['dao.image']->deleteById($id);

		$app['session']->getFlashBag()->add('success', 'L\'image à bien été supprimée');

		return $app->redirect('/admin/project/adddetails/'.$project_id);
	}
}
