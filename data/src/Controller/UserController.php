<?php

namespace Danid3\Controller;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Danid3\Domain\User;
use Danid3\Form\Type\UserType;

class UserController {

	public function addAction(Request $request, Application $app) {
		$user = new User();
		$userForm = $app['form.factory']->create(new UserType(), $user);
		$userForm->handleRequest($request);

		if ($userForm->isSubmitted() && $userForm->isValid()) {
	        // generate a random salt value
	        $salt = substr(md5(time()), 0, 23);
	        $user->setSalt($salt);
	        $plainPassword = $user->getPassword();
	        // find the default encoder
	        $encoder = $app['security.encoder.digest'];
	        // compute the encoded password
	        $password = $encoder->encodePassword($plainPassword, $user->getSalt());
	        $user->setPassword($password); 
	        $app['dao.user']->save($user);
	        $app['session']->getFlashBag()->add('success', 'The user was successfully created.');
		}
		return $app['twig']->render('user_form.html.twig', array(
			'title' => 'Créer un nouvel utilisateur',
			'userForm' =>$userForm->createView()
		));
	}

    /**
     * Edit user controller.
     *
     * @param integer $id User id
     * @param Request $request Incoming request
     * @param Application $app Silex application
     */
    public function editAction($id, Request $request, Application $app) {
        $user = $app['dao.user']->find($id);
        $userForm = $app['form.factory']->create(new UserType(), $user);
        $userForm->handleRequest($request);
        if ($userForm->isSubmitted() && $userForm->isValid()) {
            $plainPassword = $user->getPassword();
            // find the encoder for the user
            $encoder = $app['security.encoder_factory']->getEncoder($user);
            // compute the encoded password
            $password = $encoder->encodePassword($plainPassword, $user->getSalt());
            $user->setPassword($password); 
            $app['dao.user']->save($user);
            $app['session']->getFlashBag()->add('success', 'The user was succesfully updated.');
        }
        return $app['twig']->render('user_form.html.twig', array(
            'title' => 'Modifier utilisateur',
            'userForm' => $userForm->createView()));
    }

    /**
     * Delete user controller.
     *
     * @param integer $id User id
     * @param Application $app Silex application
     */
    public function deleteAction($id, Application $app) {
        // Delete the user
        $app['dao.user']->delete($id);
        $app['session']->getFlashBag()->add('success', 'The user was succesfully removed.');
        return $app->redirect('/admin');
    }
}
