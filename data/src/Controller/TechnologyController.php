<?php

namespace Danid3\Controller;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Danid3\Domain\Technology;
use Danid3\Form\Type\TechnologyType;

class TechnologyController {

	public function addAction(Request $request, Application $app) {
		$technology = new Technology();
		$technologyForm = $app['form.factory']->create(new TechnologyType(), $technology);
		$technologyForm->handleRequest($request);
		if ($technologyForm->isSubmitted() && $technologyForm->isValid()) {
			$app['dao.technology']->save($technology);
			$app['session']->getFlashBag()->add('success', 'La technologie à bien été créée.');
		}
		return $app['twig']->render('tech_service_form.html.twig', array(
			'title' => 'Nouvelle technologie',
			'techServiceForm' => $technologyForm->createView()));
	}

	public function editAction(Request $request, Application $app, $id) {
		$technology = $app['dao.technology']->findById($id);
		$technologyForm = $app['form.factory']->create(new TechnologyType(), $technology);
		$technologyForm->handleRequest($request);
		if ($technologyForm->isSubmitted() && $technologyForm->isValid()) {
			$app['dao.technology']->save($technology);
			$app['session']->getFlashBag()->add('success', 'La technologie à bien été modifiée.');
		}
		return $app['twig']->render('tech_service_form.html.twig', array(
			'title' => 'Modifier technologie',
			'techServiceForm' => $technologyForm->createView()));
	}

	public function deleteAction(Application $app, $tech_id) {
		$app['dao.projecttechnology']->deleteByTechnologyId($tech_id);
		$app['dao.technology']->delete($tech_id);
		$app['session']->getFlashBag()->add('success', 'La technologie à bien été supprimée.');

		return $app->redirect('/admin');
	}
}
