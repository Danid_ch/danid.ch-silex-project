<?php

namespace Danid3\DAO;

use Danid3\Domain\ProjectService;

class ProjectServiceDAO extends DAO
{
	private $serviceDAO;
	private $projectDAO;

	public function setServiceDAO(ServiceDAO $serviceDAO) {
		$this->serviceDAO = $serviceDAO;
	}

	public function setProjectDAO(ProjectDAO $projectDAO) {
		$this->projectDAO = $projectDAO;
	}

	protected function buildDomainObject($row) {
		$projserv = new ProjectService();

		$projserv->setId($row['projserv_id']);
		if (array_key_exists('proj_id', $row)) {
			$projectId = $row['proj_id'];
			$project = $this->projectDAO->findById($projectId);
			$projserv->setProject($project);
		}
		if (array_key_exists('serv_id', $row)) {
			$serviceId = $row['serv_id'];
			$service = $this->serviceDAO->findById($serviceId);
			$projserv->setService($service);
		}

		return $projserv;
	}

	public function findAll() {
		$sql = "SELECT * FROM projserv";
		$result = $this->getDb()->fetchAll($sql);

		$projServs = array();
		foreach ($result as $row) {
			$projServId = $row['projserv_id'];
			$projServ = $this->buildDomainObject($row);

			$project = $this->projectDAO->findById($row['proj_id']);
			$service = $this->serviceDAO->findById($row['serv_id']);

			$projServ->setProject($project);
			$projServ->setService($service);
			$projServs[$projServId] = $projServ;
		}

		return $projServs;
	}

	public function findById($id) {
		$sql = "SELECT * FROM projserv WHERE projserv_id=?";
		$result = $this->getDb()->fetchAssoc($sql, array($id));

		if ($result) {
			return $this->buildDomainObject($result);
		} else {
			throw new \Exception("No project service matching this id ".$id);
		}
	}

	public function findAllByProject($projectId) {
		$project = $this->projectDAO->findById($projectId);

		$sql = "SELECT * FROM projserv WHERE proj_id=?";
		$result = $this->getDb()->fetchAll($sql, array($projectId));

		$projServs = array();
		foreach ($result as $row) {
			$projServId = $row['projserv_id'];
			$projServ = $this->buildDomainObject($row);

			$service = $this->serviceDAO->findById($row['serv_id']);

			$projServ->setProject($project);
			$projServ->setService($service);
			$projServs[$projServId] = $projServ;
		}

		return $projServs;
	}

	public function findAllByService($serviceId) {
		$service = $this->serviceDAO->findById($serviceId);

		$sql = "SELECT * FROM projserv WHERE serv_id=?";
		$result = $this->getDb()->fetchAll($sql, array($serviceId));

		$projServs = array();
		foreach ($result as $row) {
			$projServId = $row['projserv_id'];
			$projServ = $this->buildDomainObject($row);

			$project = $this->projectDAO->findById($row['proj_id']);

			$projServ->setProject($project);
			$projServ->setService($service);
			$projServs[$projServId] = $projServ;
		}

		return $projServs;
	}

	/**
	 * Saves a service in the database
	 * @param  \Danid3\Domain\Service $service [description]
	 */
	public function save(ProjectService $projServ){
		$projServData = array(
			'proj_id' => $projServ->getProject()->getId(),
			'serv_id' => $projServ->getService()->getId()
			);

		if ($projServ->getId()) {
			$this->getDb()->update('projserv', $projServData, array('projserv_id' => $projServ->getId()));
		} else {
			$this->getDb()->insert('projserv', $projServData);
			$id = $this->getDb()->lastInsertId();
			$projServ->setId($id);
		}
	}

	public function delete($id) {
		$this->getDb()->delete('projserv', array('projserv_id' => $id));
	}

	public function deleteByServiceId($servId) {
		$this->getDb()->delete('projserv', array('serv_id' => $servId));
	}

	public function deleteByProjectId($proj_id) {
		$this->getDb()->delete('projserv', array('proj_id' => $proj_id));
	}
}
