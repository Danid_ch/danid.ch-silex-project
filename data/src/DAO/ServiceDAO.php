<?php

namespace Danid3\DAO;

use Danid3\Domain\Service;

class ServiceDAO extends DAO
{
	protected function buildDomainObject($row) {
		$service = new Service();
		$service->setId($row['serv_id']);
		$service->setName($row['serv_name']);

		return $service;
	}

	public function findById($id){
		$sql = "SELECT * FROM service WHERE serv_id=?";
		$row = $this->getDb()->fetchAssoc($sql, array($id));

		if ($row) {
			return $this->buildDomainObject($row);
		} else {
			throw new \Exception("No service matching this id ".$id);
		}
	}

	/**
	 * Returns a list of all the services sorted by id
	 * @return [array] A list of services
	 */
	public function findAll() {
		$sql = "SELECT * FROM service ORDER BY serv_id ASC";
		$result = $this->getDb()->fetchAll($sql);

		// Convert query result to an array of project objects
		$services = array();
		foreach ($result as $row) {
			$services[] = $this->buildDomainObject($row);
		}

		return $services;
	}

	public function findAllNotInProject($projectId) {
		$sql = "SELECT * FROM service WHERE serv_id NOT IN (SELECT serv_id FROM projserv WHERE proj_id=?)";
		$result = $this->getDb()->fetchAll($sql, array($projectId));

		// Convert query result to an array of project objects
		$nonRelatedservs = array();
		foreach ($result as $row) {
			$nonRelatedservs[] = $this->buildDomainObject($row);
		}

		return $nonRelatedservs;
	}

	/**
	 * Saves a service in the database
	 * @param  \Danid3\Domain\Service $service [description]
	 */
	public function save(Service $service){
		$serviceData = array(
			'serv_name' => $service->getName(),
			);

		if ($service->getId()) {
			$this->getDb()->update('service', $serviceData, array('serv_id' => $service->getId()));
		} else {
			$this->getDb()->insert('service', $serviceData);
			$service->setId($this->getDb()->lastInsertId());
		}
	}

	public function delete($servId) {
		$this->getDb()->delete('service', array('serv_id' => $servId));
	}
}
