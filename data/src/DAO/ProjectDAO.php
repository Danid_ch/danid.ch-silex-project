<?php

namespace Danid3\DAO;

use Danid3\Domain\Project;

class ProjectDAO extends DAO
{
	/**
	 * Builds a Project object based on a DB row
	 * @param  [array] $row DB row containing the data on the project
	 * @return \Danid3\Domain\Project
	 */
	protected function buildDomainObject($row) {
		$project = new Project();
		$project->setId($row['proj_id']);
		$project->setSlug($row['proj_slug']);
		$project->setName($row['proj_name']);
		$project->setDescription($row['proj_desc']);
		$project->setOnlineDate(new \DateTime($row['proj_date']));
		$project->setPublished((bool)$row['proj_publ']);

		if($row['proj_date'] !== null){
			$project->setOnlineDate(new \DateTime($row['proj_date']));
		} else {
			$project->setOnlineDate(null);
		}

		return $project;
	}

	public function findById($id){
		$sql = "SELECT * FROM project WHERE proj_id=?";
		$row = $this->getDb()->fetchAssoc($sql, array($id));

		if ($row) {
			return $this->buildDomainObject($row);
		} else {
			throw new \Exception("No project matching this id ".$id);
		}
	}

	public function findBySlug($slug){
		$sql = "SELECT * FROM project WHERE proj_slug=? AND proj_publ = 1";
		$row = $this->getDb()->fetchAssoc($sql, array($slug));

		if ($row) {
			return $this->buildDomainObject($row);
		} else {
			throw new \Exception("No project matching this slug ".$slug);
		}
	}

	/**
	 * Returns a list of all the projects sorted by online date
	 * @return [array] A list of projects
	 */
	public function findAll() {
		$sql = "SELECT * FROM project ORDER BY proj_date DESC";
		$result = $this->getDb()->fetchAll($sql);

		// Convert query result to an array of project objects
		$projects = array();
		foreach ($result as $row) {
			$projects[] = $this->buildDomainObject($row);
		}

		return $projects;
	}

	public function findAllPublished() {
		$sql = "SELECT * FROM project WHERE proj_publ = 1 ORDER BY CASE WHEN proj_date IS NULL THEN 1 ELSE 0 END, proj_date DESC";
		$result = $this->getDb()->fetchAll($sql);

		// Convert query result to an array of project objects
		$projects = array();
		foreach ($result as $row) {
			$projects[] = $this->buildDomainObject($row);
		}

		return $projects;
	}

	/**
	 * Returns a unique slug based on slug recieved
	 * @return [string] A unique slug
	 */
	public function findUniqueSlug($slug) {
		$sql = "SELECT * FROM project WHERE proj_slug LIKE ?";
		$rows = $this->getDb()->fetchAll($sql, array($slug."%"));

		if ($rows) {
			return $slug."-".count($rows);
		} else {
			return $slug;
		}
	}

	/**
	 * Saves a project in the database
	 * @param  \Danid3\Domain\Project $project [description]
	 */
	public function save(Project $project){
		// Check for unicity of slug
		$uniqueSlug = $this->findUniqueSlug($project->getSlug());

		if($project->getOnlineDate() === null) {
			$date = null;
		} else {
			$date = $project->getOnlineDate()->format('Y-m-d');
		}

		$projectData = array(
			'proj_slug' => $uniqueSlug,
			'proj_name' => $project->getName(),
			'proj_date' => $date,
			'proj_desc' => $project->getDescription(),
			'proj_publ' => $project->getPublished(),
			);

		if ($project->getId()) {
			$this->getDb()->update('project', $projectData, array('proj_id' => $project->getId()));
		} else {
			$this->getDb()->insert('project', $projectData);
			$project->setId($this->getDb()->lastInsertId());
		}
	}

	public function delete($proj_id) {
		$this->getDb()->delete('project', array('proj_id' => $proj_id));
	}
}
