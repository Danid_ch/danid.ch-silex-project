<?php

namespace Danid3\DAO;

use Danid3\Domain\Image;

class ImageDAO extends DAO
{
	private $projectDAO;

	public function setProjectDAO(ProjectDAO $projectDAO) {
		$this->projectDAO = $projectDAO;
	}

	public function buildDomainObject($row) {
		$image = new Image();
		$image->setId($row['img_id']);
		$image->setTitle($row['img_title']);
		$image->setAlt($row['img_alt']);
		$image->setFilename($row['img_filename']);

		if(array_key_exists('proj_id', $row)) {
			$projectId = $row['proj_id'];
			$project = $this->projectDAO->findById($projectId);
			$image->setProject($project);
		}

		return $image;
	}

	public function findById($imageId) {
		$sql = "SELECT * FROM image WHERE img_id=?";
		$row = $this->getDb()->fetchAssoc($sql, array($imageId));

		if ($row) {
			return $this->buildDomainObject($row);
		} else {
			return new \Exception("The image with this id doesn't existe");
		}
	}

	public function findAll() {
		$sql = "SELECT * FROM image ORDER BY img_id ASC";
		$result = $this->getDb()->fetchAll($sql);

		$images = array();
		foreach ($result as $row) {
			$images[] = $this->buildDomainObject($row);
		}

		return $images;
	}

	public function findAllByProject($projectId) {
		$sql = "SELECT * FROM image WHERE proj_id=?";
		$result = $this->getDb()->fetchAll($sql, array($projectId));

		$images = array();
		foreach ($result as $row) {
			$images[] = $this->buildDomainObject($row);
		}

		return $images;
	}

	public function save(Image $image) {
		$imageData = array(
			'img_title' => $image->getTitle(),
			'img_alt' => $image->getAlt(),
			'img_filename' => $image->getFilename(),
			'proj_id' => $image->getProject()->getId()
		);

		if($image->getId()) {
			$this->getDb()->update('image', $imageData, array('img_id' => $image->getId()));
		} else {
			$this->getDb()->insert('image', $imageData);
			$id = $this->getDb()->lastInsertId();
			$image->setId($id);
		}
	}

	public function deleteById($id) {
		$this->getDb()->delete('image', array('img_id' => $id));
	}

	public function deleteByProjectId($proj_id) {
		$this->getDb()->delete('image', array('proj_id' => $proj_id));
	}
}
