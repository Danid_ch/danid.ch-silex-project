<?php

namespace Danid3\DAO;

use Danid3\Domain\ProjectTechnology;

class ProjectTechnologyDAO extends DAO
{
	private $technologyDAO;
	private $projectDAO;

	public function setTechnologyDAO(TechnologyDAO $technologyDAO) {
		$this->technologyDAO = $technologyDAO;
	}

	public function setProjectDAO(ProjectDAO $projectDAO) {
		$this->projectDAO = $projectDAO;
	}

	protected function buildDomainObject($row) {
		$projtech = new ProjectTechnology();

		$projtech->setId($row['projtech_id']);
		if (array_key_exists('proj_id', $row)) {
			$projectId = $row['proj_id'];
			$project = $this->projectDAO->findById($projectId);
			$projtech->setProject($project);
		}
		if (array_key_exists('tech_id', $row)) {
			$technologyId = $row['tech_id'];
			$technology = $this->technologyDAO->findById($technologyId);
			$projtech->setTechnology($technology);
		}

		return $projtech;
	}

	public function findAll() {
		$sql = "SELECT * FROM projtech";
		$result = $this->getDb()->fetchAll($sql);

		$projTechs = array();
		foreach ($result as $row) {
			$projTechId = $row['projtech_id'];
			$projTech = $this->buildDomainObject($row);

			$project = $this->projectDAO->findById($row['proj_id']);
			$technology = $this->technologyDAO->findById($row['tech_id']);

			$projTech->setProject($project);
			$projTech->setTechnology($technology);
			$projTechs[$projTechId] = $projTech;
		}

		return $projTechs;
	}

	public function findById($id) {
		$sql = "SELECT * FROM projtech WHERE projtech_id=?";
		$result = $this->getDb()->fetchAssoc($sql, array($id));

		if ($result) {
			return $this->buildDomainObject($result);
		} else {
			throw new \Exception("No project technology matching this id ".$id);
		}
	}

	public function findAllByProject($projectId) {
		$project = $this->projectDAO->findById($projectId);

		$sql = "SELECT * FROM projtech WHERE proj_id=?";
		$result = $this->getDb()->fetchAll($sql, array($projectId));

		$projTechs = array();
		foreach ($result as $row) {
			$projTechId = $row['projtech_id'];
			$projTech = $this->buildDomainObject($row);

			$technology = $this->technologyDAO->findById($row['tech_id']);

			$projTech->setProject($project);
			$projTech->setTechnology($technology);
			$projTechs[$projTechId] = $projTech;
		}

		return $projTechs;
	}

	public function findAllByTechnology($technologyId) {
		$technology = $this->technologyDAO->findById($technologyId);

		$sql = "SELECT * FROM projtech WHERE tech_id=?";
		$result = $this->getDb()->fetchAll($sql, array($technologyId));

		$projTechs = array();
		foreach ($result as $row) {
			$projTechId = $row['projtech_id'];
			$projTech = $this->buildDomainObject($row);

			$project = $this->projectDAO->findById($row['proj_id']);

			$projTech->setProject($project);
			$projTech->setTechnology($technology);
			$projTechs[$projTechId] = $projTech;
		}

		return $projTechs;
	}

	/**
	 * Saves a technology in the database
	 * @param  \Danid3\Domain\Technology $technology [description]
	 */
	public function save(ProjectTechnology $projTech){
		$projTechData = array(
			'proj_id' => $projTech->getProject()->getId(),
			'tech_id' => $projTech->getTechnology()->getId()
			);

		if ($projTech->getId()) {
			$this->getDb()->update('projtech', $projTechData, array('projtech_id' => $projTech->getId()));
		} else {
			$this->getDb()->insert('projtech', $projTechData);
			$id = $this->getDb()->lastInsertId();
			$projTech->setId($id);
		}
	}

	public function delete($id) {
		$this->getDb()->delete('projtech', array('projtech_id' => $id));
	}

	public function deleteByTechnologyId($techId) {
		$this->getDb()->delete('projtech', array('tech_id' => $techId));
	}

	public function deleteByProjectId($proj_id) {
		$this->getDb()->delete('projtech', array('proj_id' => $proj_id));
	}
}
