<?php

namespace Danid3\DAO;

use Danid3\Domain\Technology;

class TechnologyDAO extends DAO
{
	protected function buildDomainObject($row) {
		$technology = new Technology();
		$technology->setId($row['tech_id']);
		$technology->setName($row['tech_name']);

		return $technology;
	}

	public function findById($id){
		$sql = "SELECT * FROM technology WHERE tech_id=?";
		$row = $this->getDb()->fetchAssoc($sql, array($id));

		if ($row) {
			return $this->buildDomainObject($row);
		} else {
			throw new \Exception("No technology matching this id ".$id);
		}
	}

	/**
	 * Returns a list of all the technologies sorted by id
	 * @return [array] A list of technologies
	 */
	public function findAll() {
		$sql = "SELECT * FROM technology ORDER BY tech_id ASC";
		$result = $this->getDb()->fetchAll($sql);

		// Convert query result to an array of project objects
		$technologies = array();
		foreach ($result as $row) {
			$technologies[] = $this->buildDomainObject($row);
		}

		return $technologies;
	}

	public function findAllNotInProject($projectId) {
		$sql = "SELECT * FROM technology WHERE tech_id NOT IN (SELECT tech_id FROM projtech WHERE proj_id=?)";
		$result = $this->getDb()->fetchAll($sql, array($projectId));

		// Convert query result to an array of project objects
		$technologies = array();
		foreach ($result as $row) {
			$technologies[] = $this->buildDomainObject($row);
		}

		return $technologies;
	}

	/**
	 * Saves a technology in the database
	 * @param  \Danid3\Domain\Technology $technology [description]
	 */
	public function save(Technology $technology){
		$technologyData = array(
			'tech_name' => $technology->getName(),
			);

		if ($technology->getId()) {
			$this->getDb()->update('technology', $technologyData, array('tech_id' => $technology->getId()));
		} else {
			$this->getDb()->insert('technology', $technologyData);
			$technology->setId($this->getDb()->lastInsertId());
		}
	}

	public function delete($techId) {
		$this->getDb()->delete('technology', array('tech_id' => $techId));
	}
}
