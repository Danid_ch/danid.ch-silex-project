<?php

namespace Danid3\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class ContactType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('subject', 'text')
			->add('message', 'textarea')
			->add('lastName', 'text')
			->add('firstName', 'text')
			->add('email', 'email');
	}

	public function getName()
	{
		return 'contact';
	}
}
