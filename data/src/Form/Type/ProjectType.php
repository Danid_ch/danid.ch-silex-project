<?php

namespace Danid3\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class ProjectType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options) {
		$builder->add('name', 'text')
			->add('description', 'textarea')
			->add('onlineDate', 'date')
			->add('isPublished', 'checkbox', array('required' => false));
	}

	public function getName(){
		return 'project';
	}
}
