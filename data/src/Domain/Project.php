<?php

namespace Danid3\Domain;

class Project
{
	/**
	 * Project id
	 * @var integer
	 */
	private $id;

	/**
	 * Project name slugified
	 * @var [string]
	 */
	private $slug;

	/**
	 * Project name
	 * @var string
	 */
	private $name;

	/**
	 * Project description
	 * @var text
	 */
	private $description;

	/**
	 * Date the project went online
	 * @var date
	 */
	private $onlineDate;

	/**
	 * Project published status
	 * @var bollean
	 */
	private $published;

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getSlug(){
    	return $this->slug;
    }

    public function setSlug($slug){
    	$this->slug = $slug;
    }
	
	public function setName($name){
		$this->name = $name;
	}

	public function getName(){
		return $this->name;
	}

	public function setDescription($description){
		$this->description = $description;
	}

	public function getDescription(){
		return $this->description;
	}

	public function setOnlineDate($date){
		$this->onlineDate = $date;
	}

	public function getOnlineDate(){
		return $this->onlineDate;
	}

	public function getPublished(){
		return $this->published;
	}

	public function setPublished($boolean) {
		$this->published = $boolean;
	}
}
