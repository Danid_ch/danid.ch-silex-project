<?php

namespace Danid3\Domain;

class Image
{
	/**
	 * Image id
	 * @var [integer]
	 */
	private $id;

	/**
	 * Image filename
	 * @var [string]
	 */
	private $filename;

	/**
	 * Image alternative text
	 * @var [string]
	 */
	private $alt;

	/**
	 * Image title
	 * @var [string]
	 */
	private $title;

	/**
	 * Project to which the image is attached
	 * @var \Danid3\Domain\Project
	 */
	private $project;

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }
	
	public function setFilename($filename) {
		$this->filename = $filename;
	}

	public function getFilename() {
		return $this->filename;
	}

	public function setAlt($alt) {
		$this->alt = $alt;
	}

	public function getAlt() {
		return $this->alt;
	}

	public function setTitle($title) {
		$this->title = $title;
	}

	public function getTitle() {
		return $this->title;
	}

	public function setProject(Project $project) {
		$this->project = $project;
	}

	public function getProject() {
		return $this->project;
	}
}
