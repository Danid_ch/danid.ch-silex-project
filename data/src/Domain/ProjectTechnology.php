<?php

namespace Danid3\Domain;

class ProjectTechnology {

	/**
	 * ProjectTechnology id
	 * @var integer
	 */
	private $id;

	/**
	 * Associated project
	 * @var \Danid3\Domain\Project
	 */
	private $project;

	/**
	 * Associated technology
	 * @var \Danid3\Domain\Technology
	 */
	private $technology;

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

	public function setProject(Project $project) {
		$this->project = $project;
	}

	public function getProject() {
		return $this->project;
	}

	public function setTechnology(Technology $technology) {
		$this->technology = $technology;
	}

	public function getTechnology() {
		return $this->technology;
	}
}
