<?php

namespace Danid3\Domain;

class ProjectService {

	/**
	 * ProjectService id
	 * @var integer
	 */
	private $id;

	/**
	 * Associated project
	 * @var \Danid3\Domain\Project
	 */
	private $project;

	/**
	 * Associated service
	 * @var \Danid3\Domain\Service
	 */
	private $service;

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

	public function setProject(Project $project) {
		$this->project = $project;
	}

	public function getProject() {
		return $this->project;
	}

	public function setService(Service $service) {
		$this->service = $service;
	}

	public function getService() {
		return $this->service;
	}
}
