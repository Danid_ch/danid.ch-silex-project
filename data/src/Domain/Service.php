<?php

namespace Danid3\Domain;

class Service
{
	/**
	 * Service id
	 * @var [integer]
	 */
	private $id;

	/**
	 * Service name
	 * @var [string]
	 */
	private $name;

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }
	
	public function setName($name){
		$this->name = $name;
	}

	public function getName(){
		return $this->name;
	}
}
