<?php

use Symfony\Component\Debug\ErrorHandler;
use Symfony\Component\Debug\ExceptionHandler;

// Register global error and exception handlers
ErrorHandler::register();
ExceptionHandler::register();

// Register service providers
$app->register(new Silex\Provider\DoctrineServiceProvider());
$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => __DIR__.'/../views',
));
$app->register(new Silex\Provider\SessionServiceProvider());
$app->register(new Silex\Provider\SwiftmailerServiceProvider(), array('swiftmailer.options' => $app['swiftmailer.options']));
$app->register(new Silex\Provider\FormServiceProvider());
$app->register(new Silex\Provider\TranslationServiceProvider());
$app->register(new Silex\Provider\UrlGeneratorServiceProvider());
$app->register(new Silex\Provider\MonologServiceProvider(), array(
    'monolog.logfile' => __DIR__.'/../var/logs/danid3.log',
    'monolog.name' => 'Danid3',
    'monolog.level' => $app['monolog.level']
));
$app->register(new Silex\Provider\ServiceControllerServiceProvider());
$app->register(new Cocur\Slugify\Bridge\Silex\SlugifyServiceProvider());
$app->register(new Silex\Provider\SecurityServiceProvider(), array(
    'security.firewalls' => array(
        'secured' => array(
            'pattern' => '^/',
            'anonymous' => true,
            'logout' => true,
            'form' => array('login_path' => '/login', 'check_path' => '/login_check'),
            'users' => $app->share(function () use ($app) {
                return new Danid3\DAO\UserDAO($app['db']);
            }),
        ),
    ),
    'security.role_hierarchy' => array(
        'ROLE_ADMIN' => array('ROLE_USER'),
    ),
    'security.access_rules' => array(
        array('^/admin', 'ROLE_ADMIN'),
    ),
));
$app->register(new Silex\Provider\HttpFragmentServiceProvider());
if (isset($app['debug']) && $app['debug']) {
    $app->register(new Silex\Provider\WebProfilerServiceProvider(), array(
        'profiler.cache_dir' => __DIR__.'/../var/cache/profiler'
    ));
}

// Register services
$app['dao.project'] = $app->share(function ($app) {
	return new Danid3\DAO\ProjectDAO($app['db']);
});

$app['dao.technology'] = $app->share(function ($app) {
    return new Danid3\DAO\TechnologyDAO($app['db']);
});

$app['dao.service'] = $app->share(function ($app) {
    return new Danid3\DAO\ServiceDAO($app['db']);
});

$app['dao.image'] = $app->share(function ($app) {
    $imageDAO = new Danid3\DAO\ImageDAO($app['db']);
    $imageDAO->setProjectDAO($app['dao.project']);
    return $imageDAO;
});

$app['dao.projecttechnology'] = $app->share(function ($app) {
    $projecttechnologyDAO = new Danid3\DAO\ProjectTechnologyDAO($app['db']);
    $projecttechnologyDAO->setProjectDAO($app['dao.project']);
    $projecttechnologyDAO->setTechnologyDAO($app['dao.technology']);
    return $projecttechnologyDAO;
});

$app['dao.projectservice'] = $app->share(function ($app) {
    $projectserviceDAO = new Danid3\DAO\ProjectServiceDAO($app['db']);
    $projectserviceDAO->setProjectDAO($app['dao.project']);
    $projectserviceDAO->setServiceDAO($app['dao.service']);
    return $projectserviceDAO;
});

$app['dao.user'] = $app->share(function ($app) {
    return new Danid3\DAO\UserDAO($app['db']);
});

$app['mailer'] = $app->share(function ($app) {
    return new \Swift_Mailer($app['swiftmailer.transport']);
});
