<?php

// Home
$app->get('/', "Danid3\Controller\HomeController::indexAction");
$app->get('/portfolio', "Danid3\Controller\HomeController::portfolioAction");
$app->get('/qui-suis-je', "Danid3\Controller\HomeController::meAction");
$app->get('/qr', "Danid3\Controller\HomeController::meAction");
$app->get('/qr.html', "Danid3\Controller\HomeController::meAction");
$app->match('/contact', "Danid3\Controller\HomeController::contactAction");

// Admin routes
$app->get('/admin', "Danid3\Controller\AdminController::indexAction");
$app->get('/login', "Danid3\Controller\AdminController::loginAction")->bind('login');

// User routes
$app->match('/admin/user/add', "Danid3\Controller\UserController::addAction");
$app->match('/admin/user/edit/{id}', "Danid3\Controller\UserController::editAction");
$app->get('/admin/user/delete/{id}', "Danid3\Controller\UserController::deleteAction");

// Project routes
$app->match('/admin/project/add', "Danid3\Controller\ProjectController::addAction");
$app->get('/admin/project/adddetails/{id}', "Danid3\Controller\ProjectController::addDetailsAction");
$app->match('/admin/project/edit/{id}', "Danid3\Controller\ProjectController::editAction");
$app->get('/admin/project/delete/{id}', "Danid3\Controller\ProjectController::deleteAction");
$app->get('/project/{slug}', "Danid3\Controller\ProjectController::showAction");
$app->get('/admin/project/publish/{id}', "Danid3\Controller\ProjectController::publishAction");
$app->get('/admin/project/unpublish/{id}', "Danid3\Controller\ProjectController::unpublishAction");

// Image routes
$app->match('/admin/image/add/{proj_id}', "Danid3\Controller\ImageController::addAction");
$app->match('/admin/image/edit/{id}', "Danid3\Controller\ImageController::editAction");
$app->get('/admin/image/delete/{id}', "Danid3\Controller\ImageController::deleteAction");

// Technology routes
$app->match('/admin/technology/add', "Danid3\Controller\TechnologyController::addAction");
$app->match('/admin/technology/edit/{id}', "Danid3\Controller\TechnologyController::editAction");
$app->get('/admin/technology/delete/{tech_id}', "Danid3\Controller\TechnologyController::deleteAction");

// Project technology routes
$app->get('/admin/projtech/add/{id_proj}/{id_tech}', "Danid3\Controller\ProjectTechnologyController::addAction");
$app->get('/admin/projtech/delete/{projtech_id}', "Danid3\Controller\ProjectTechnologyController::deleteAction");

// Service routes
$app->match('/admin/service/add', "Danid3\Controller\ServiceController::addAction");
$app->match('/admin/service/edit/{id}', "Danid3\Controller\ServiceController::editAction");
$app->get('/admin/service/delete/{serv_id}', "Danid3\Controller\ServiceController::deleteAction");

// Project service routes
$app->get('/admin/projserv/add/{proj_id}/{serv_id}', "Danid3\Controller\ProjectServiceController::addAction");
$app->get('/admin/projserv/delete/{projserv_id}', "Danid3\Controller\ProjectServiceController::deleteAction");
