DROP TABLE IF EXISTS project;
DROP TABLE IF EXISTS technology;
DROP TABLE IF EXISTS service;
DROP TABLE IF EXISTS projtech;
DROP TABLE IF EXISTS image;
DROP TABLE IF EXISTS projserv;
DROP TABLE IF EXISTS projtech;
DROP TABLE IF EXISTS user;

CREATE TABLE project (
  proj_id int(11) NOT NULL AUTO_INCREMENT,
  proj_slug varchar(255) NOT NULL,
  proj_name text NOT NULL,
  proj_desc longtext NOT NULL,
  proj_date date DEFAULT 0,
  proj_publ boolean DEFAULT 0,
  PRIMARY KEY (proj_id),
  UNIQUE (proj_slug)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE technology (
  tech_id int(11) NOT NULL AUTO_INCREMENT,
  tech_name varchar(255) NOT NULL,
  PRIMARY KEY (tech_id),
  UNIQUE (tech_name)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ;

CREATE TABLE service (
  serv_id int(11) NOT NULL AUTO_INCREMENT,
  serv_name varchar(255) NOT NULL,
  PRIMARY KEY (serv_id),
  UNIQUE (serv_name)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ;

CREATE TABLE projtech (
  projtech_id int(11) NOT NULL AUTO_INCREMENT,
  tech_id int(11) NOT NULL,
  proj_id int(11) NOT NULL,
  PRIMARY KEY (projtech_id),
  UNIQUE (tech_id, proj_id)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ;

CREATE TABLE projserv (
  projserv_id int(11) NOT NULL AUTO_INCREMENT,
  serv_id int(11) NOT NULL,
  proj_id int(11) NOT NULL,
  PRIMARY KEY (projserv_id),
  UNIQUE (serv_id, proj_id)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ;

CREATE TABLE image (
  img_id int(11) NOT NULL AUTO_INCREMENT,
  img_filename varchar(255) NOT NULL,
  img_title varchar(255) NOT NULL,
  img_alt varchar(255) NOT NULL,
  proj_id int(11) NOT NULL,
  PRIMARY KEY (img_id),
  UNIQUE (img_filename)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ;

CREATE TABLE user (
    user_id integer NOT NULL AUTO_INCREMENT,
    user_name varchar(50) NOT NULL,
    user_password varchar(88) NOT NULL,
    user_salt varchar(23) NOT NULL,
    user_role varchar(50) NOT NULL,
    PRIMARY KEY (user_id),
    UNIQUE (user_name)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
