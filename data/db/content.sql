# Projects
INSERT INTO `project` (`proj_id`, `proj_name`, `proj_desc`, `proj_date`, `proj_publ`, `proj_slug`) VALUES
(1, 'Musée Bolo – Rétropublicité', 'Mise en forme d’une interface pour une projection lors d’un événement au musée Bolo de l’EPFL. A partir d’un thème, rétropublicité, il a fallu imaginer une ligne graphique pour cette projection de publicité vielle de plusieurs années.', NULL, 0, 'musee-bolo-retropublicite'),
(2, "Weihnachtskommers 2010", 'Transformation de concepts en dessins vectorielles pour l’impression sur support tissu pour un évenement. De l’idée d’un couteau suisse pour vétérinaire il a fallu élaborer un dessins pouvant être imprimé sur des t-shirts pour financer une soirée de la faculté vétérinaire de l’université de Berne. Le deuxième T-shirt a été décalqué depuis un dessin du célèbre «Simon’s Cat» avec l’accord des titulaires des droits d’auteurs.', NULL, 0, 'weihnachtskommers-2010'),
(3, "Label'attitude", 'Projet de cours autour du thème: «Les métiers verts». Ce travail de groupe a été un aboutissement de notre formation qui a permis d’utiliser toutes les compétences apprises pendant les trois années de Bachelor.', '2012-04-26', 0, 'label-attitude'),
(4, "Matty's Barber Shop and Karry's Salon", 'Site internet et site mobile pour un coiffeur de Stafford Springs dans le Connecticut. Il s’agissait de donner une visibilité à ce commerçant avec un site rudimentaire permettant aux habitant de la régions de connaitre ses horaires d’ouvertures ainsi que ses tarifs.', '2012-08-23', 0, 'matty-barber-shop-and-karry-salon'),
(5, 'Sites One Partner SA', "Site internet et site mobile pour l'entreprise One Partner SA (partenaire Abacus). Il s’agissait de renouveler le site internet viellissant de se prestataire informatique et de faire un site vitrine pour sa dernière solution Sky365.", '2014-11-20', 0, 'sites-one-partner-sa');

# Technologies
INSERT INTO `technology` (`tech_id`, `tech_name`) VALUES
(1, 'HTML'),
(2, 'HTML5'),
(3, 'CSS2'),
(4, 'CSS3'),
(5, 'Bootstrap'),
(6, 'JavaScript'),
(7, 'jQuery'),
(8, 'jQuery Mobile'),
(9, 'AJAX'),
(10, 'Base de données'),
(11, 'Symfony 2'),
(12, 'Silex'),
(13, 'WordPress'),
(14, 'PHP 5'),
(15, 'Vectorisation depuis un dessin');

# Project-technologies
INSERT INTO `projtech` (`proj_id`, `tech_id`) VALUES
(1, 3),
(2, 15),
(3, 10),
(3, 6),
(3, 7),
(4, 2),
(4, 4),
(4, 6),
(4, 7),
(4, 8),
(5, 2),
(5, 4),
(5, 5),
(5, 7),
(5, 14);

# Services
INSERT INTO `service`(`serv_id`, `serv_name`) VALUES
(1, 'Développement site internet'),
(2, 'Développement site mobile'),
(3, 'Graphisme'),
(4, 'Vectorisation'),
(5, 'Préparation de fichier pour l\'impression');

# Project-Services
INSERT INTO `projserv` (`proj_id`, `serv_id`) VALUES
(1, 3),
(2, 4),
(2, 5),
(3, 1),
(4, 1),
(4, 2),
(4, 3),
(5, 1),
(5, 2),
(5, 3);
