# New version of my personal website

For this project I will be using the Silex framework and eventually migrate to Symfony framework later on.

Starting from now this project even has a SensioLabInsight Medal:
[![SensioLabsInsight](https://insight.sensiolabs.com/projects/8bcde37b-c51b-4841-8a38-a9922e571984/big.png)](https://insight.sensiolabs.com/projects/8bcde37b-c51b-4841-8a38-a9922e571984)

## Still to do at this point
* Correct project DAO, Controller etc for isPublished boolean
