<?php

require_once __DIR__.'/../data/vendor/autoload.php';

$app = new Silex\Application();

require __DIR__.'/../data/app/config/dev.php';
require __DIR__.'/../data/app/app.php';
require __DIR__.'/../data/app/routes.php';

$app->run();
